#!/usr/bin/python

__author__ = 'arwed'

from Adjazenzmatrix import Adjazenzmatrix
from Tkinter import *

am = Adjazenzmatrix()


def show_longest_path():
    paths = "Longest paths:\n"
    start = input_longest_from.get()
    end = input_longest_to.get()
    for p in am.longest_path(start, end):
        for l in p:
            paths += l + "->"
        paths = paths[:-2] + "\n"
    longest_path.set(paths)


def show_shortest_path():
    paths = "Shortest paths:\n"
    start = input_shortest_from.get()
    end = input_shortest_to.get()
    for p in am.shortest_path(start, end):
        for l in p:
            paths += l + "->"
        paths = paths[:-2] + "\n"
    shortest_path.set(paths)


def show_matrix():
    am.load_from_file(input_path.get())
    if inverted.get() == 1:
        ~am
    if output_path.get():
        output = open(output_path.get(), "w")
        output.write("%s" % am.get_matrix_latex())
        output.close()
    matrix.set(am)
    frame.grid(columnspan=2)


def check_if_path_valid(key):
    try:
        with open(input_path.get() + key.char):
            input_file.configure(bg="green")
    except EnvironmentError:
        input_file.configure(bg="red")

print("Path not found")

gui = Tk()
gui.title("Adjazenzmatrixgenerator")
output_path = StringVar()
input_path = StringVar()
input_label = Label(gui, text="Enter location of the graph / relations:").grid(row=0, sticky=E)
input_file = Entry(gui, textvariable=input_path, bg="red")
input_file.bind("<Key>", check_if_path_valid)
input_file.grid(row=0, column=1)

Label(gui, text="Enter a location to save the Adjazenzmatrix:").grid(row=1, sticky=E)
Entry(gui, textvariable=output_path).grid(row=1, column=1)
inverted = IntVar()
Checkbutton(gui, text="inverted", variable=inverted).grid(columnspan=2)
Button(gui, text="Generate Matrix", command=show_matrix).grid(columnspan=2)

matrix = StringVar()
Label(gui, textvariable=matrix).grid(columnspan=2)

frame = Frame(gui)

Label(frame, text="Longest path from:").grid(row=0)
input_longest_from = StringVar()
Entry(frame, textvariable=input_longest_from).grid(column=1, row=0)
Label(frame, text="to:").grid(row=0, column=2)
input_longest_to = StringVar()
Entry(frame, textvariable=input_longest_to).grid(column=3, row=0)
Button(frame, text="Find", command=show_longest_path).grid(column=4, row=0)

Label(frame, text="Shortest path from to:").grid(row=1)
input_shortest_from = StringVar()
Entry(frame, textvariable=input_shortest_from).grid(column=1, row=1)
Label(frame, text="to:").grid(column=2, row=1)
input_shortest_to = StringVar()
Entry(frame, textvariable=input_shortest_to).grid(column=3, row=1)
Button(frame, text="Find", command=show_shortest_path).grid(column=4, row=1)


paths_frame = Frame(frame)

longest_path = StringVar()
Label(paths_frame, textvariable=longest_path).grid()

shortest_path = StringVar()
Label(paths_frame, textvariable=shortest_path).grid()
paths_frame.grid(columnspan=5)

gui.mainloop()


