__author__ = 'arwed'
import re


class Adjazenzmatrix(object):
    """
    Simulates an Adjazenzmatrix.
    """

    def __init__(self):
        self.nodes = []
        self.edges = set()
        self.matrix = []

    def __invert__(self):
        edges = set([tuple([z[1], z[0]]) for z in self.edges])
        self.edges = edges

    def __str__(self):
        string = "\t"
        for n in self.nodes:
            string += "%s\t" % n
        string = string[:-1] + "\n"
        self.generate_matrix()
        for i in range(0, len(self.nodes)):
            string += "%s\t" % self.nodes[i]
            for j in self.matrix:
                string += "1\t" if j[i] else "0\t"
            string = string[:-1] + "\n"
        return string

    def get_matrix_latex(self):
        string = "\\begin{tabular}{" + (len(self.nodes)+1) * "|c"\
                 + "|}" + "\n\\hline\n &"
        for n in self.nodes:
            string += "%s & " % n
        string = string[:-2] + "\\tabularnewline\n\\hline\n"
        self.generate_matrix()
        for i in range(0, len(self.nodes)):
            string += "%s & " % self.nodes[i]
            for j in self.matrix:
                string += "1 & " if j[i] else "0 & "
            string = string[:-2] + "\\tabularnewline\n\\hline\n"

        string += "\\end{tabular}"
        return string

    def add_edge(self, edge):
        """Add an Edge / relation between two nodes.
        :type self: list
        """
        self.edges |= set([tuple(edge)])
        self.add_node(edge[0], edge[1])

    def add_node(self, *nodes):
        for n in nodes:
            if n in self.nodes:
                pass
            else:
                self.nodes += [n]

    def generate_matrix(self):
        """
        Generates a matrix from the nodes and edges.
        When an edge is added to the matrix,
        this method must be called to refresh the matrix.
        """
        self.matrix = [[False] * len(self.nodes) for _ in self.nodes]
        for e in self.edges:
            self.matrix[self.nodes.index(e[0])][self.nodes.index(e[1])] = [True]

    def load_from_file(self, path):
        """
        Takes a path to a text file, where the edges of the are saved.
        Pattern: [node1, node2], [node3, node1] ...
        :param path:
        :return:
        """
        try:
            with open(path, 'r') as data:
                matrix_data = re.findall(r"\[(.*?)\]", data.read())
                for s in matrix_data:
                    s = s.replace(", ", ",")
                    self.add_edge(s.split(","))
                return True
        except EnvironmentError:
            return False

    def longest_path(self, start=False, end=False):
        """
        Finds the longest paths.
        You can specify an start and end point.
        :param start: String
        :param end: String
        :return: List
        """
        path = [[]]
        for x in self.__trans_closure():
            if start and end:
                if not (x[0] == start and x[-1] == end):
                    continue
            if len(x) > len(path[0]):
                path = [x]
            elif len(x) == len(path[0]):
                path += [x]
        return path

    def shortest_path(self, start=False, end=False):
        """
        Finds the shortest paths.
        You can specify an start and end point.
        :param start: String
        :param end: String
        :return: List
        """
        path = []
        for x in self.__trans_closure():
            if start and end:
                if not (x[0] == start and x[-1] == end):
                    continue
            if len(path) == 0:
                path = [x]
                continue
            if len(x) < len(path[0]):
                path = [x]
            elif len(x) == len(path[0]):
                path += [x]
        return path

    def __trans_closure(self):
        """
        This calculates the transitive closure of the graph.
        The transitive closure contains all paths of this Graph.
        :return: transitive closure
        """
        p = self.edges
        while True:
            old_p = p.copy()
            p |= self.__path_product(self.edges, p)
            if p == old_p:
                return p

    def __path_product(self, p, q):
        return set([self.__add(x, y) for x in p for y in q
                    if x[-1] == y[0] and self.__no_cyclic(x, y)])

    @staticmethod
    def __add(p, q):
        """
        Concatenates two tuples together. p + q
        :param p: tuple
        :param q: tuple
        :return: tuple
        """
        return tuple(p + q[1:])

    @staticmethod
    def __no_cyclic(l1, l2):
        """
        Can be used to check if a path is cyclic.
        :param l1: tuple
        :param l2: tuple
        :return: bool
        """
        return len(set(l1) & set(l2)) == 1