# README #

This repository contains the independent studies of Tobias Dorra and Arwed Mett which are located  in */doc* - Folder.

In the  *src* - Folder is a program to calculate an Adjazenzmatrix by a given amount of edges.

## Prerequisites: ##
* Python 2.8
* PDF - Viewer

## Using the Program ##
To run the program you have to execute the __init__.py file.

Then you have to specify an input file (relative path to the input file) where all the edges of your desired graph are stored.
### Example ###
../Graphentheorie/relations.txt
### Format of the file ###
The Edges have to be defined by the following syntax.

The squared brackets indicate an edge. So in order to define a Graph you have to define multiple edges.

For Example: [Node1, Node2], [Node2, Node3] ...

### Saving the Adjazenzmatrix ###
You can also specify an output path where your generated Adjazenzmatrix will be saved after you generate it.

The matrix will be saved in Latex code.